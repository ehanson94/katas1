function oneThroughTwenty() {
    const numbers = []

    // Your code goes below
    for(let i=1; i<=20; i++){
        numbers.push(i)
    }
    return numbers;
}
console.log('One through twenty: \n', oneThroughTwenty());

function evensToTwenty() {
    const numbers = []
    
    // Your code goes below
    for(let i=2; i<=20; i += 2){
        numbers.push(i)
    }
    return numbers;
}
console.log('Evens through 20: \n', evensToTwenty());

function oddsToTwenty() {
    const numbers = []
    
    // Your code goes below
    for(let i=1; i<=20; i+=2){
        numbers.push(i)
    }
    return numbers;
}
console.log('Odds through twenty: \n', oddsToTwenty());

function multiplesOfFive() {
    const numbers = []
    
    // Your code goes below
    for(let i=5; i<=100; i+=5){
        numbers.push(i)
    }
    return numbers;
}
console.log('Multiples of five: \n', multiplesOfFive());

function squareNumbers() {
    const numbers = []
    
    // Your code goes below
    for(let i=1; i<=100; i++){
        let sqRoot = Math.sqrt(i)
        if (Math.floor(sqRoot) !== sqRoot){
            continue;
        }
        numbers.push(i)
    }
    return numbers;
}
console.log('Square numbers: \n', squareNumbers());

function countingBackwards() {
    const numbers = []
    
    // Your code goes below
    for(let i=20; i>=1; i--){
        numbers.push(i)
    }
    return numbers;
}
console.log('Counting down: \n', countingBackwards());

function evenNumbersBackwards() {
    const numbers = []
    
    // Your code goes below
    for(let i=20; i>=2; i-=2){
        numbers.push(i)
    }
    return numbers;
}
console.log('Evens counting down: \n', evenNumbersBackwards());

function oddNumbersBackwards() {
    const numbers = []
    
    // Your code goes below
    for(let i=19; i>=1; i-=2){
        numbers.push(i)
    }
    return numbers;
}
console.log('Odds counting down: \n', oddNumbersBackwards());

function multiplesOfFiveBackwards() {
    const numbers = []
    
    // Your code goes below
    for(let i=100; i>=5; i-=5){
        numbers.push(i)
    }
    return numbers;
}
console.log('Counting down multiples of five: \n', multiplesOfFiveBackwards());

function squareNumbersBackwards() {
    const numbers = []
    
    // Your code goes below
    for(let i=100; i>0; i--){
        let sqRoot = Math.sqrt(i);
        if(Math.floor(sqRoot) !== sqRoot){
            continue;
        }
        numbers.push(i)
    }
    return numbers;
}
console.log('Counting down square numbers: \n', squareNumbersBackwards());